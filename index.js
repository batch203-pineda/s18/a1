// console.log("Hello World!");

/* 3. Create a function which will be able to add two numbers.
- Numbers must be provided as arguments.
- Display the result of the addition in our console.
- function should only display result. It should not return anything.
 */

    function addFunc(num1, num2) {
        let sum = num1 + num2;
        console.log("Displayed sum of "+num1+" and "+num2);
        console.log(sum);
    }
        addFunc(5,15);



/* 4. Create a function which will be able to subtract two numbers.
- Numbers must be provided as arguments.
- Display the result of subtraction in our console.
- function should only display result. It should not return anything.
 */

    function subtractFunc(num1, num2) {
        let sub = num1 - num2;
        console.log("Displayed difference of "+num1+" and "+num2);
        console.log(sub);
    }   
        subtractFunc(20,5);



/* 7. Create a function which will be able to multiply two numbers.
- Numbers must be provided as arguments. 
- Return the result of the multiplication.
 */

let product = function (num1, num2) {
    let mul = num1 * num2;
    console.log("The product of "+num1+" and "+num2);
    return mul;
}
console.log(product(50,10)); 



/* 8. Create a function which will be able to divide two numbers.
- Numbers must be provided as arguments.
- Return the result of the division.
 */

function divFunc(num1 , num2) {
    let div = num1 / num2;
    console.log("The quotient of "+num1+" and "+num2);
    return div;
}
console.log(divFunc(50,10));



/* 11. Create a function which will be able to get the total area of a circle from a provided radius.
- a number should be provided as an argument.
- look up the formula for calculating the area of a circle with a provided/given radius.
- look up the use of the exponent operator.
- return the result of the area calculation.

12. Create a new variable called circleArea.
- This variable should be able to receive and store the result of the circle area calculation.
- Log the value of the circleArea variable in the console.
 */

function circle(radius) {
    circleArea = Math.PI * radius * radius;
    console.log("The result of getting the area of a cirle with "+radius+" radius");
    return circleArea;
}

console.log(circle(15));



/* 13. Create a function which will be able to get a total average of four numbers.
- 4 numbers should be provided as an argument.
- look up the formula for calculating the average of numbers.
- return the result of the average calculation.

14. Create a new variable called averageVar.
- This variable should be able to receive and store the result of the average calculation
- Log the value of the averageVar variable in the console.
 */

function averageVar(num1, num2, num3, num4) {
    
    let sum = num1+num2+num3+num4;
    let avg = sum/4;
    console.log("The average of "+num1+","+num2+","+num3+" and "+num4+":");
    return avg;
}

console.log(averageVar(20,40,60,80));

/* 15. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.

- this function should take 2 numbers as an argument, your score and the total score.
- First, get the percentage of your score against the total. You can look up the - formula to get the percentage.
- Using a relational operator, check if your score percentage is greater than or equal to 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
- return the value of the variable isPassed.
- This function should return a boolean.

16. Create a new variable called isPassingScore.
- This variable should be able to receive and store the boolean result of the function.
- Log the value of the isPassingScore variable in the console.
 */

function percentage(num1, num2) {
    
    let isPassed = (num1/num2)*100>75;
    let isPassingScore = (num1/num2)*100>=75;
    console.log("Is "+num1+"/"+num2+" a passing score?");
    return isPassed;
}

console.log(percentage(38,50));

